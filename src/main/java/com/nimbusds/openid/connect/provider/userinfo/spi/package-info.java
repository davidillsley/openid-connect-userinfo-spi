/**
 * OpenID Connect UserInfo SPI.
 */
package com.nimbusds.openid.connect.provider.userinfo.spi;

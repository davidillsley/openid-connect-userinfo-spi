package com.nimbusds.openid.connect.provider.userinfo.spi;


import java.util.List;
import java.util.Properties;
import java.util.Set;

import com.nimbusds.langtag.LangTag;

import com.nimbusds.oauth2.sdk.id.Subject;

import com.nimbusds.openid.connect.sdk.claims.UserInfo;


/**
 * OpenID Connect UserInfo provider.
 *
 * @author Vladimir Dzhuvinov
 */
public interface UserInfoProvider {


	/**
	 * Initialises the OpenID Connect UserInfo claims provider. This method
	 * is called after the UserInfo SPI implementation is loaded.
	 *
	 * @param props Properties required for the initialisation. Must not be
	 *              {@code null}.
	 *
	 * @throws Exception If initialisation failed.
	 */
	public void init(final Properties props)
		throws Exception;


	/**
	 * Returns the names of the supported OpenID Connect UserInfo claims.
	 *
	 * <p>Example:
	 *
	 * <pre>
	 * name
	 * email
	 * email_verified
	 * </pre>
	 *
	 * @return The supported UserInfo claim names. Should not include the
	 *         reserved {@code sub} (subject) UserInfo claim name.
	 */
	public Set<String> supportedClaims();


	/**
	 * Gets the UserInfo for the specified subject.
	 *
	 * @param subject       The subject. Must not be {@code null}.
	 * @param claims        The names of the requested UserInfo claims,
	 *                      with optional language tags. Must not be
	 *                      {@code null}.
	 * @param claimsLocales The preferred languages and scripts for the
	 *                      claims to return, {@code null} if not
	 *                      specified.
	 *
	 * @return The UserInfo, {@code null} if the subject wasn't found.
	 *
	 * @throws Exception If retrieval of the UserInfo failed.
	 */
	public UserInfo getUserInfo(final Subject subject, final Set<String> claims, final List<LangTag> claimsLocales)
		throws Exception;


	/**
	 * Shuts down the OpenID Connect UserInfo claims provider. This method
	 * is called on OpenID Connect Provider (OP) shutdown.
	 *
	 * @throws Exception If proper shutdown failed.
	 */
	public void shutdown()
		throws Exception;
}


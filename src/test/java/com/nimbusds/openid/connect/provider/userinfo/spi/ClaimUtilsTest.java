package com.nimbusds.openid.connect.provider.userinfo.spi;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.nimbusds.langtag.LangTag;
import junit.framework.TestCase;


/**
 * Tests the claim utils class.
 *
 * @author Vladimir Dzhuvinov
 */
public class ClaimUtilsTest extends TestCase {


	public void testApplyLangTags()
		throws Exception {

		Set<String> claims = new HashSet<String>();
		claims.add("name");
		claims.add("profile");

		Set<String> expandedClaims = ClaimUtils.applyLangTags(claims, null);
		assertEquals(claims, expandedClaims);

		List<LangTag> locales = new ArrayList<LangTag>();

		expandedClaims = ClaimUtils.applyLangTags(claims, locales);
		assertEquals(claims, expandedClaims);

		locales.add(LangTag.parse("bg-BG"));
		locales.add(LangTag.parse("en-US"));

		expandedClaims = ClaimUtils.applyLangTags(claims, locales);

		assertTrue(expandedClaims.contains("name#bg-BG"));
		assertTrue(expandedClaims.contains("name#en-US"));
		assertTrue(expandedClaims.contains("profile#bg-BG"));
		assertTrue(expandedClaims.contains("profile#en-US"));
		assertEquals(4, expandedClaims.size());
	}
}

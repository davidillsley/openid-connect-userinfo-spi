OpenID Connect UserInfo SPI

Copyright (c) NimbusDS.com, 2013


README


Service Provider Interface (SPI) for OpenID Connect UserInfo claims. The SPI is
designed to enable an OpenID Connect Provider (OP) to aggregate claims from
multiple UserInfo providers.

Summary of the UserInfo SPI:

 * Allows for initialisation of the UserInfo provider from Java properties,
   passed via a configuration file or otherwise.

 * Allows for inspection of the claim names that the UserInfo provider
   supports.

 * Allows implementations to release resources on OP shutdown.


Questions or comments? Email support@nimbusds.com
